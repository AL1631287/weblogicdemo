<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>WebLogic Demo</title>
		<base href="<%=request.getContextPath()%>/"> 
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style_v2.css" rel="stylesheet">
		
		<link href="css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<script src="js/libs/jquery/jquery-3.2.1.min.js"></script>
		<script src="js/libs/bootstrap/bootstrap.min.js"></script>
		
		<script src="js/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<link href="css/bootstrap-multiselect.css" rel="stylesheet">
		
		<link href="css/bootsnav/animate.css" rel="stylesheet">
		<link href="css/bootsnav/bootsnav.css" rel="stylesheet">
		<script src="js/libs/bootsnav.js"></script>
		
		<script src="js/libs/bootstrap-maxlength.js"></script>
		<script src="js/libs/bootstrap-checkbox.js"></script>
		
		<link rel="shortcut icon" href="images/icon.ico" />
	</head>
	<body class="">
		<div class="loader"></div>
		<div class="page-wrap">
			<nav class="navbar navbar-default navbar-sidebar bootsnav no-full">
				<div class="container">
					<div class="navbar-header">
					   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" aria-expanded="true">
							<span class="fa fa-bars"></span>
						</button>
						<a class="navbar-brand" href=""><span class="fa fa-home"></span> Index</a>
					</div>
				
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav menu" data-in="fadeInDown" data-out="fadeOutUp">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown">Demo</a>
								<ul class="dropdown-menu">
									<li><a href="content">Content!</a></li>
								    <li><a href="oof">A pdf file!</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<div class="pageheader">
				<div class="container">
					<h2 class="title">Spring-Boot MVC</h2>
					<p>DemoApp</p>
				</div>
			</div>
			<section class="container">
				<c:import url="${vue}" />
			</section>
		</div>
		
		<script>
			$(document).ready(function() {
				
				$('.cancelCheckbox').css("display","block");
				
				//Once page has loaded we remove the loading thingy
				$(".loader").fadeOut();
				
				//Maxlength plugin
				$("input[maxlength]").maxlength();
			});
		</script>
	</body>
</html>