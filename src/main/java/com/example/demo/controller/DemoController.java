package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DemoController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger("DEMO");
	
	protected final static String filename = "oof.pdf";

	@RequestMapping(value={"", "/"})
	public String index() {
		return "index";
	}
	
	@RequestMapping(value= {"/content"})
	public String content() {
		return "index2";
	}
	
	@RequestMapping(value="/oof")
	public ResponseEntity<byte[]> oof() {
		LOGGER.info("OOF");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);
	    headers.setContentDispositionFormData(filename, filename);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	    return new ResponseEntity<byte[]>(new byte[0], headers, HttpStatus.OK);
	}
	
}
